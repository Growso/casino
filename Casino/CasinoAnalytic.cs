﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Casino;

namespace CasinoAnalysis
{
    class CasinoAnalytic : ICasinoAnalytic
    {
        public IEnumerable<Bet> Bets
        {
            get
            {
                var resultList = new List<Bet>();
                string path = Path.Combine(Environment.CurrentDirectory, "Log\\Log.txt");
                using (var streamReader = new StreamReader(path))
                {
                    while (!streamReader.EndOfStream)
                    {
                        string str = streamReader.ReadLine();
                        if (str != null && str.Contains("BET"))
                        {
                            resultList.Add(ParseBetStringToBet(str));
                        }
                    }
                }
                return resultList;
            }
        }

        private static Bet ParseBetStringToBet(string str)
        {
            var tmp = str.Split(' ');
            //А че с балансом?Вообще не понятно, где его взять.Я не буду его брать.
            User user = new User {Name = tmp[1]};
            
            GameCode code;
            Enum.TryParse(tmp[2], out code); 

            Bet newBet = new Bet
            {
                User = user,
                Value = Convert.ToInt32(tmp[3].Substring(4)),
                GameCode = code,
                Date = DateTime.Parse(tmp[0].Substring(1, tmp[0].Length - 2))
            };
            return newBet;
        }

        public IEnumerable<Deposite> Deposites
        {
            get
            {
                var resultList = new List<Deposite>();
                string path = Path.Combine(Environment.CurrentDirectory, "Log\\Log.txt");
                using (var streamReader = new StreamReader(path))
                {
                    while (!streamReader.EndOfStream)
                    {
                        string str = streamReader.ReadLine();
                        if (str != null && str.Contains("DEPOSITE"))
                        {
                            resultList.Add(ParseDepositeStringToDeposite(str));
                        }
                    }
                    streamReader.Close();
                }
                return resultList;
            }
        }

        private static Deposite ParseDepositeStringToDeposite(string str)
        {
            var tmp = str.Split(' ');
            //А че с балансом? Вообще не понятно, где его взять.Я не буду его брать.
            User user = new User { Name = tmp[1] };
            
            Deposite newDeposite = new Deposite
            {
                User = user,
                Value = Convert.ToInt32(tmp[2].Substring(9)), 
                Date = DateTime.Parse(tmp[0].Substring(1, tmp[0].Length - 2))
            };
            return newDeposite;
        }

        public IEnumerable<GameResults> GamesResults
        {
            get
            {
                var resultList = new List<GameResults>();
                string path = Path.Combine(Environment.CurrentDirectory, "Log\\Log.txt");
                using (var streamReader = new StreamReader(path))
                {
                    while (!streamReader.EndOfStream)
                    {
                        string str = streamReader.ReadLine();
                        if (str != null && !str.Contains("BET") && !str.Contains("DEPOSITE"))
                        {
                            resultList.Add(ParseGameResStringToGameRes(str));
                        }
                    }
                }
                return resultList;
            }
        }

        private GameResults ParseGameResStringToGameRes(string str)
        {
            var tmp = str.Split(' ');
            //А че с балансом? Вообще не понятно, где его взять.Я не буду его брать.
            User user = new User { Name = tmp[1] };

            GameCode code;
            Enum.TryParse(tmp[2], out code); 

            GameResultStatus status;
            Enum.TryParse(tmp[3], out status); 
            
            if (code.Equals(GameCode.BJ))
            {
                return new BlackJackGameResults(
                    DateTime.Parse(tmp[0].Substring(1, tmp[0].Length - 2)), user, status, Convert.ToInt32(tmp[4]),
                    Convert.ToInt32(tmp[5]), Convert.ToInt32(tmp[6]));
            }
            if (code.Equals(GameCode.Dice))
            {
                return new DiceGameResults(
                    DateTime.Parse(tmp[0].Substring(1, tmp[0].Length - 2)), user, status, Convert.ToInt32(tmp[4]),
                    Convert.ToInt32(tmp[5]), Convert.ToInt32(tmp[6]));
            }
            return new RouletGameResults(DateTime.Parse(tmp[0].Substring(1, tmp[0].Length - 2)), user, status,
                Convert.ToInt32(tmp[4]), tmp[5], Convert.ToInt32(tmp[6]));
        }
    
        public Bet MaxBet()
        {
            return Bets.Aggregate((max, bet) => bet.Value > max.Value ? bet : max);
        }

        public Bet MaxBet(GameCode code)
        {
            return
                Bets.Where(bet => bet.GameCode.Equals(code)).Aggregate((max, bet) => bet.Value > max.Value ? bet : max);
        }

        public int AverageDeposite()
        {
            return Deposites.Sum(dep => dep.Value) / Deposites.Count();
        }

        public int AverageDeposite(User user)
        {
            return Deposites.Where(dep => user.Equals(dep.User)).Sum(dep => dep.Value)/
                   Deposites.Count(dep => user.Equals(dep.User));
        }
        
        public IEnumerable<Deposite> TopDeposites(int count)
        {
            return Deposites.OrderByDescending(dep => dep.Value).Take(count);
        }

        public IEnumerable<Deposite> RichestClients(int count)
        {
            return Deposites.OrderByDescending(dep1 =>
                Deposites.Where(dep2 => dep2.User.Equals(dep1.User)).Sum(dep2 => dep2.Value)
                ).Take(count);
        }

        public GameCode MaxProfitGame(out int amount)
        {
            var tmp = GamesResults.Where(res => res.Status.Equals(GameResultStatus.Loose)).GroupBy(res => res.GameCode);
            var resultTuple = tmp.Aggregate(Tuple.Create(tmp.First().Sum(res => res.BalanceChange), tmp.First().Key),
                (tupleWithMaximalSum, group) =>
                {
                    int sum = group.Sum(res => res.BalanceChange);
                    return sum > tupleWithMaximalSum.Item1 ? Tuple.Create(sum, group.Key) : tupleWithMaximalSum;
                });
            amount = resultTuple.Item1;
            return resultTuple.Item2;
        }

        public User MostLuckyUser(GameCode game)
        {
            var tmp = GamesResults.Where(res => res.GameCode.Equals(game)).GroupBy(res => res.User);
            var resultTuple =
                tmp.Aggregate(
                    Tuple.Create(
                        tmp.First().Where(res => res.Status.Equals(GameResultStatus.Win)).Sum(res => res.BalanceChange)/
                        tmp.First().Where(res => res.Status.Equals(GameResultStatus.Win)).Sum(res => res.BalanceChange),
                        tmp.First().Key),
                    (tupleWithMaximalRatio, group) =>
                    {
                        int ratio =
                            group.Where(res => res.Status.Equals(GameResultStatus.Win)).Sum(res => res.BalanceChange)/
                            group.Where(res => res.Status.Equals(GameResultStatus.Win)).Sum(res => res.BalanceChange);
                        return ratio > tupleWithMaximalRatio.Item1
                            ? Tuple.Create(ratio, group.Key)
                            : tupleWithMaximalRatio;
                    });
            return resultTuple.Item2;
        }

        public User MaxLuckyUser()
        {
            var tmp = GamesResults.GroupBy(res => res.User);
            var resultTuple =
                tmp.Aggregate(
                    Tuple.Create(
                        tmp.First().Where(res => res.Status.Equals(GameResultStatus.Win)).Sum(res => res.BalanceChange)/
                        tmp.First().Where(res => res.Status.Equals(GameResultStatus.Win)).Sum(res => res.BalanceChange),
                        tmp.First().Key),
                    (tupleWithMaximalRatio, group) =>
                    {
                        int ratio =
                            group.Where(res => res.Status.Equals(GameResultStatus.Win)).Sum(res => res.BalanceChange)/
                            group.Where(res => res.Status.Equals(GameResultStatus.Win)).Sum(res => res.BalanceChange);
                        return ratio > tupleWithMaximalRatio.Item1
                            ? Tuple.Create(ratio, group.Key)
                            : tupleWithMaximalRatio;
                    });
            return resultTuple.Item2;
        }

        public int UserDeposite(User user, DateTime date)
        {
            return Deposites.Where(dep => date.Equals(dep.Date)).Sum(dep => dep.Value);
        }

        public IEnumerable<int> ZeroBasedBalanceHistoryExchange(User user)
        {
            var tmp = GamesResults.Where(res => res.User.Equals(user));
            var resList = new List<int>();
            tmp.Aggregate(0,
                (last, res) =>
                {
                    if (res.Status.Equals(GameResultStatus.Win))
                    {
                        last += res.BalanceChange;
                    }
                    else
                    {
                        last -= res.BalanceChange;
                    }
                    resList.Add(last);
                    return last;
                });
            return resList;
        }

        public IEnumerable<int> ZeroBasedBalanceHistoryExchange(User user, DateTime from)
        {
            var tmp = GamesResults.Where(res => res.User.Equals(user)).Where(res => res.Date > from);
            var resList = new List<int>();
            tmp.Aggregate(0,
                (last, res) =>
                {
                    if (res.Status.Equals(GameResultStatus.Win))
                    {
                        last += res.BalanceChange;
                    }
                    else
                    {
                        last -= res.BalanceChange;
                    }
                    resList.Add(last);
                    return last;
                });
            return resList;
        }

        public Dictionary<DateTime, int> ProfitByMonths()
        {
            //Я так и не понял, в каком порядке сортировать? По месяцам? Или как?
            var tmp = GamesResults.Where(res => res.Status.Equals(GameResultStatus.Loose))
                .OrderBy(res => res.Date.Month)
                .GroupBy(res => res.Date.Month);
            return tmp.Aggregate(new Dictionary<DateTime, int>(),
                (dict, group) =>
                {
                    dict.Add(group.First().Date, group.Sum(res => res.BalanceChange));
                    return dict;
                });
        }

        public Dictionary<DateTime, int> GamesCountByMounths()
        {
            var tmp = GamesResults.GroupBy(res => res.Date.Month);
            return tmp.Aggregate(new Dictionary<DateTime, int>(),
                (dict, group) =>
                {
                    dict.Add(group.First().Date, group.Count());
                    return dict;
                });
        }

        public Dictionary<DateTime, int> GamesCountByMounths(GameCode game)
        {
            var tmp = GamesResults.Where(res => res.GameCode.Equals(game)).GroupBy(res => res.Date.Month);
            return tmp.Aggregate(new Dictionary<DateTime, int>(),
                (dict, group) =>
                {
                    dict.Add(group.First().Date, group.Count());
                    return dict;
                });
        }

        public Dictionary<DateTime, int> NewUsersByMounths()
        {
            var tmp = GamesResults.GroupBy(res => res.Date.Month);
            return tmp.Aggregate(new Dictionary<DateTime, int>(),
                (dict, group) =>
                {
                    var userList = group.Aggregate(new List<User>(),
                        (usersDict, res) =>
                        {
                            if (!usersDict.Contains(res.User))
                            {
                                usersDict.Add(res.User);
                            }
                            return usersDict;
                        });
                    dict.Add(group.First().Date, userList.Count);
                    return dict;
                });
        }

        public Dictionary<DateTime, int> NewUsersByMounths(GameCode game)
        {
            var tmp = GamesResults.Where(res => res.GameCode.Equals(game)).GroupBy(res => res.Date.Month);
            return tmp.Aggregate(new Dictionary<DateTime, int>(),
                (dict, group) =>
                {
                    var userList = group.Aggregate(new List<User>(),
                        (usersDict, res) =>
                        {
                            if (!usersDict.Contains(res.User))
                            {
                                usersDict.Add(res.User);
                            }
                            return usersDict;
                        });
                    dict.Add(group.First().Date, userList.Count);
                    return dict;
                });
        }
    }
}
