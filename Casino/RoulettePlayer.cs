﻿namespace Casino
{
    public class RoulettePlayer : Player
    {
        public RoulettePlayer(Player player) : base(player.MyName, player.MyCash)
        {
        }

        public int Number { get; set; }

        public string RouletteInfo()
        {
            switch (Number)
            {
                case -1:
                    return "red";
                case -2:
                    return "black";
            }
            return Number.ToString();
        }
    }
}
