﻿using System;
using System.IO;
using System.Linq;

namespace Casino
{
    static class Utilities
    {
        public static int GameDigit(string str)
        {
            int digit = ReadPositiveNumber(str);
            while (digit < 1 || digit > 3)
            {
                Console.WriteLine("Incorrect digit, try again");
                digit = ReadPositiveNumber(Console.ReadLine());
            }
            return digit;
        }

        public static int ReadPositiveNumber(string str)
        {
            int digit;
            while (!Int32.TryParse(str, out digit) || (digit < 0))
            {
                Console.WriteLine("Incorrect number, try again");
                str = Console.ReadLine();
            }
            return digit;
        }

        public static bool AnswerN(string str)
        {
            while (str == null || (!str.Equals("y") && !str.Equals("n")))
            {
                Console.WriteLine("Incorrect answer, try again");
                str = Console.ReadLine();
            }
            return str.Equals("n");
        }

        public static string ReadCorrectName(string str)
        {
            while (String.IsNullOrEmpty(str) || !str.All(Char.IsLetter))
            {
                Console.WriteLine("Incorrect name, try again");
                str = Console.ReadLine();
            }
            return str;
        }

        public static void AddCash(Player player)
        {
            Console.WriteLine("Do you want to add money y/n?");
            if (!AnswerN(Console.ReadLine()))
            {
                Console.WriteLine("Enter cash you want to add");
                int addCash = ReadPositiveNumber(Console.ReadLine());
                player.AddOrSubCash(addCash);
            }
            Console.WriteLine(player);
            PlayerLog(player);
        }

        private static void PlayerLog(Player player)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Log\\Log.txt");
            using (var streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine("[{2}] {0} DEPOSITE:{1}", player.MyName, player.MyCash, DateTimeOffset.Now);
            }
        }

        public static int ReadRouletteNumber(string str)
        {
            var result = ReadPositiveNumber(str);
            while (result > 36)
            {
                Console.WriteLine("Incorrect number, try again");
                result = ReadPositiveNumber(Console.ReadLine());
            }
            return result;
        }

        public static void WriteRouletteResult(int rouletteNumber)
        {
            if (rouletteNumber == 0)
            {
                Console.WriteLine("The result is ZERO");
            }
            else
            {
                Console.WriteLine(IsOdd(rouletteNumber) ? "The result is {0}, red" : "The result is {0}, black",
                    rouletteNumber);
            }
        }


        public static bool IsEven(int rouletteNumber)
        {
            return rouletteNumber % 2 == 0;
        }

        public static bool IsOdd(int rouletteNumber)
        {
            return rouletteNumber % 2 == 1;
        }

        public static string ChooseColorRedOrBlack(string str)
        {
            while (str == null || (!str.Equals("r") && !str.Equals("b")))
            {
                Console.WriteLine("Incorrect color, try again");
                str = Console.ReadLine();
            }
            return str.Equals("r") ? "r" : "b";
        }

        public static int MakeBet(Player player, byte gameCode)
        {
            Console.WriteLine("Enter bet please!");
            int bet = ReadPositiveNumber(Console.ReadLine());
            while (bet > player.MyCash)
            {
                Console.WriteLine("You have not got enough money!");
                AddCash(player);
                Console.WriteLine("Enter correct bet!");
                bet = ReadPositiveNumber(Console.ReadLine());
            }
            BetLog(player, bet, gameCode);
            return bet;
        }

        private static void BetLog(Player player, int bet, byte gameCode)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Log\\Log.txt");
            using (var streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine("[{3}] {0} {1} BET:{2}", player.MyName, gameCode, bet, DateTimeOffset.Now);
            }
        }
    }
}
