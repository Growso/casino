﻿
namespace Casino
{
    public class DicePlayer : Player
    {
        public DicePlayer(Player player) : base(player.MyName, player.MyCash)
        {
        }

        public int Sum { get; set; }

    }
}
