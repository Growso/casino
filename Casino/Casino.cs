﻿using System;
using System.Collections.Generic;
using System.IO;


namespace Casino
{
    class Casino
    {
        private Dictionary<String,Int32> myPlayers; 

        public Casino()
        {
            myPlayers = new Dictionary<string, int>(); 
            ReadFromLog(myPlayers);
        }

        public void PlayingCasino()
        {
            Console.WriteLine("Hello player!");
            Console.WriteLine("Enter your name, please!");
            string name = Utilities.ReadCorrectName(Console.ReadLine());
            int cash;
            if (!myPlayers.ContainsKey(name))
            {
                myPlayers.Add(name, 0);
                cash = 0;
            }
            else
            {
                cash = myPlayers[name];
            }
            Player player = new Player(name, cash); 
            
            while (true)
            {
                Console.WriteLine(player);

                Utilities.AddCash(player);
                
                Console.WriteLine("Enter digit of game you want to play:");
                Console.WriteLine("1.Roulette");
                Console.WriteLine("2.BlackJack");
                Console.WriteLine("3.Dice");
                int gameIdentifier = Utilities.GameDigit(Console.ReadLine());
                switch (gameIdentifier)
                {
                    case 1:
                        RoulettePlayer playerR = new RoulettePlayer(player);
                        RouletteGame gameR = new RouletteGame(playerR);
                        gameR.PlayGame();
                        player.MyCash = playerR.MyCash;
                        break;
                    case 2:
                        BlackJackPlayer playerBj = new BlackJackPlayer(player);
                        BlackJackGame gameBj = new BlackJackGame(playerBj);
                        gameBj.PlayGame();
                        player.MyCash = playerBj.MyCash;
                        break;
                    case 3:
                        DicePlayer playerD = new DicePlayer(player);
                        DiceGame gameD = new DiceGame(playerD);
                        gameD.PlayGame();
                        player.MyCash = playerD.MyCash;
                        break;
                }
                Console.WriteLine("Do you want to continue playing casino y/n?");
                if (Utilities.AnswerN(Console.ReadLine())) break;
            }
            myPlayers[player.MyName] = player.MyCash;
            WriteToLog(myPlayers);
            Console.WriteLine("Goodbye!");
            Console.ReadKey();
        }

        private void WriteToLog(Dictionary<string, int> dictionary)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Log\\PlyaersLog.txt");

            using (var streamWriter = new StreamWriter(path, true))
            {
                foreach (var element in dictionary)
                {
                    streamWriter.WriteLine("{0} {1}", element.Key, element.Value);
                }
            }
        }

        private static void ReadFromLog(IDictionary<string, int> dictionary)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Log\\PlyaersLog.txt");
            using (var streamReader = new StreamReader(path))
            {
                while (!streamReader.EndOfStream)
                {
                    string str = streamReader.ReadLine();
                    if (str != null)
                    {
                        string[] nameAndCash = str.Split(' ');
                        dictionary.Add(nameAndCash[0], Convert.ToInt32(nameAndCash[1]));
                    }
                }
            }
        }
    }
}
