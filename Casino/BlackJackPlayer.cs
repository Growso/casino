﻿using System;

namespace Casino
{
    public class BlackJackPlayer : Player
    {
        public int[] MyCards { get; private set; }
        private int myAmountOfCards;

        public BlackJackPlayer(Player player) : base(player.MyName, player.MyCash)
        {
            MyCards = new int[10];
            myAmountOfCards = 0;
        }

        public int Sum()
        {
            int result = 0;
            for (int i = 0; i < myAmountOfCards; i++)
            {
                result += MyCards[i];
            }
            return result;
        }

        public void ClearCards()
        {
            MyCards = new int[10];
            myAmountOfCards = 0;
        }

        public string PlayerCards() 
        {
            string result = MyName + " cards is "; 
            for (int i = 0; i < myAmountOfCards; i++)
            {
                result += MyCards[i] + " ";
            }
            result += ",sum is " + Sum();
            return result; 
        }

        public void AddCard(int num)
        {
            MyCards[myAmountOfCards] = num;
            myAmountOfCards++;
        }
    }
}
