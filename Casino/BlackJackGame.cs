﻿using System;
using System.IO;

namespace Casino
{
    public class BlackJackGame : IGame
    {
        public BlackJackGame(BlackJackPlayer player)
        {
            Player = player;
            Casino = new BlackJackPlayer(new Player("Casino", 0));
        }

        public BlackJackPlayer Player { get; private set; }
        public BlackJackPlayer Casino { get; private set; }

        public int Bet { get; set; }

        public void PlayGame()
        {
            while (true)
            {
                Bet = Utilities.MakeBet(Player, 2);
                Random random = new Random();
                
                Player.AddCard(random.Next(2, 11));
                Player.AddCard(random.Next(2, 11));
                Console.WriteLine(Player.PlayerCards());

                Console.WriteLine("More?");
                while (!Utilities.AnswerN(Console.ReadLine()))
                {
                    Player.AddCard(random.Next(2, 11));
                    Console.WriteLine(Player.PlayerCards());
                    if (Player.Sum() > 21) break;
                    Console.WriteLine("More?");
                }

                Casino.AddCard(random.Next(2, 11));
                Casino.AddCard(random.Next(2, 11));
                while (Casino.Sum() < 18) Casino.AddCard(random.Next(2, 11));
                Console.WriteLine(Casino.PlayerCards());

                if ((Player.Sum() > Casino.Sum() || Casino.Sum() > 21) && (Player.Sum() < 22))
                {
                    Console.WriteLine("You win!   +${0}", Bet);
                    Player.AddOrSubCash(Bet);
                    GameLog(Player, Casino, true);
                }
                else
                {
                    Console.WriteLine("You loose!   -${0}", Bet);
                    Player.AddOrSubCash(-Bet);
                    GameLog(Player, Casino, false);
                }
                Player.ClearCards();
                Casino.ClearCards();

                Console.WriteLine("Do you want to continue playing BlackJack y/n?");
                if (Utilities.AnswerN(Console.ReadLine())) break;
            }
        }

        private void GameLog(BlackJackPlayer player, BlackJackPlayer casino, Boolean win)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Log\\Log.txt");

            using (var streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine(win ? "[{4}] {0} 2 Win +{1} {2} {3}" : "[{4}] {0} 2 Loose -{1} {2} {3}",
                    player.MyName, Bet, player.Sum(), casino.Sum(), DateTimeOffset.Now);
            }
        }
    }
}
