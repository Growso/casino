﻿using System;
using System.IO;

namespace Casino
{
    public class RouletteGame : IGame
    {
        public RoulettePlayer Player { get; private set; }
        public RoulettePlayer Casino { get; private set; }

        public RouletteGame(RoulettePlayer player)
        {
            Player = player;
            Casino = new RoulettePlayer(new Player("Casino", 0));
        }

        public int Bet{ get; set; }

        public void PlayGame()
        {
            while (true)
            {
                Bet = Utilities.MakeBet(Player, 1);
                Random random = new Random();
                Casino.Number = random.Next(0, 36);
                Console.WriteLine("Do you want choose color y/n?");
                
                if (!Utilities.AnswerN(Console.ReadLine()))
                {
                    Console.WriteLine("Choose color r/b");
                    string userColor = Utilities.ChooseColorRedOrBlack(Console.ReadLine());

                    if (userColor.Equals("r"))
                    {
                        Player.Number = -1;
                    }
                    else
                    {
                        Player.Number = -2;
                    }

                    Utilities.WriteRouletteResult(Casino.Number);

                    if ((Player.Number == -1 && Utilities.IsOdd(Casino.Number))
                        || (Player.Number == -2 && Utilities.IsEven(Casino.Number)))
                    {
                        Console.WriteLine("You win!   +${0}", Bet);
                        Player.AddOrSubCash(Bet);
                        GameLog(Player, Casino, true);
                    }
                    else
                    {
                        Console.WriteLine("You loose!   -${0}", Bet);
                        Player.AddOrSubCash(-Bet);
                        GameLog(Player, Casino, false);
                    }
                }
                else
                {
                    Console.WriteLine("Choose number from 0 to 36");
                    int userNumber = Utilities.ReadRouletteNumber(Console.ReadLine());

                    Player.Number = userNumber;

                    Utilities.WriteRouletteResult(Casino.Number);

                    if (Player.Number == Casino.Number)
                    {
                        Bet *= 3;
                        Console.WriteLine("You win!   +${0}", Bet);
                        Player.AddOrSubCash(Bet);
                        GameLog(Player, Casino, true);
                    }
                    else
                    {
                        Console.WriteLine("You loose!   -${0}", Bet);
                        Player.AddOrSubCash(-Bet);
                        GameLog(Player, Casino, false);
                    }
                }
                
                Console.WriteLine("Do you want to continue playing roulette y/n?");
                if (Utilities.AnswerN(Console.ReadLine())) break;
            }
        }

        private void GameLog(RoulettePlayer player, RoulettePlayer casino, Boolean win)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Log\\Log.txt");

            using (var streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine(win ? "[{4}] {0} 1 Win +{1} {2} {3}" : "[{4}] {0} 1 Loose -{1} {2} {3}",
                    player.MyName, Bet, player.RouletteInfo(),
                    casino.Number, DateTimeOffset.Now);
            }
        }
    }
}
