﻿using System;
using System.IO;

namespace Casino
{
    public class DiceGame : IGame
    {
        public DiceGame(DicePlayer player)
        {
            Player = player;
            Casino = new DicePlayer(new Player("Casino", 0));
        }

        public DicePlayer Player { get; private set; }
        public DicePlayer Casino { get; private set; }

        public int Bet { get; set; }

        public void PlayGame()
        {
            while (true)
            {
                Bet = Utilities.MakeBet(Player, 3);
                Console.WriteLine("Enter the expected sum");
                Player.Sum = Utilities.ReadPositiveNumber(Console.ReadLine());

                Random random = new Random();
                Casino.Sum = random.Next(2, 12);

                Console.WriteLine("Dropped, sum is {0}", Casino.Sum);
                if (Player.Sum == Casino.Sum)
                {
                    Console.WriteLine("You win!   +${0}", Bet);
                    Player.AddOrSubCash(Bet);
                    GameLog(Player, Casino, true);
                }
                else
                {
                    Console.WriteLine("You loose!   -${0}", Bet);
                    Player.AddOrSubCash(-Bet);
                    GameLog(Player, Casino, false);
                }
                Console.WriteLine("Do you want to continue playing dice y/n?");
                if (Utilities.AnswerN(Console.ReadLine())) break;
            }
        }

        private void GameLog(DicePlayer player, DicePlayer casino, Boolean win)
        {
            string path = Path.Combine(Environment.CurrentDirectory, "Log\\Log.txt");
            using (var streamWriter = new StreamWriter(path, true))
            {
                streamWriter.WriteLine(win ? "[{4}] {0} 3 Win +{1} {2} {3}" : "[{4}] {0} 3 Loose -{1} {2} {3}",
                    player.MyName, Bet, player.Sum, casino.Sum, DateTimeOffset.Now);
            }
        }
    }
}
