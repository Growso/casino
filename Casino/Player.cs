﻿namespace Casino
{
    public class Player
    {
        public Player(string myName, int myCash)
        {
            MyName = myName;
            MyCash = myCash;
        }

        public string MyName { get; private set; }
        public int MyCash { get; set; }

        public void AddOrSubCash(int amount)
        {
            MyCash += amount;
        }

        public override string ToString()
        {
            return "Player " + MyName + " have $" + MyCash;
        }
    }
}
