﻿namespace Casino
{
    internal interface IGame
    {
        int Bet { get; set; }

        void PlayGame();
    }
}
